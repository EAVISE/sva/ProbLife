% Define grid params
row(0).
row(1).
row(2).
col(0).
col(1).
col(2).

% Define the cells
cell(X,Y) :- row(X), col(Y).

% Set the initially alive cells (this example, just a straight line)
initAlive(1,0).
initAlive(1,1).
initAlive(1,2).

% Define which cells are neighbours
neighbour(X,Y,X2,Y2) :- X2 is X - 1, Y2 is Y - 1, cell(X2,Y2).
neighbour(X,Y,X2,Y) :- X2 is X - 1, cell(X2,Y).
neighbour(X,Y,X2,Y2) :- X2 is X - 1, Y2 is Y + 1, cell(X2,Y2).
neighbour(X,Y,X,Y2) :- Y2 is Y - 1, cell(X,Y2).
neighbour(X,Y,X,Y2) :- Y2 is Y + 1, cell(X,Y2).
neighbour(X,Y,X2,Y2) :- X2 is X + 1, Y2 is Y - 1, cell(X2,Y2).
neighbour(X,Y,X2,Y) :- X2 is X + 1, cell(X2,Y).
neighbour(X,Y,X2,Y2) :- X2 is X + 1, Y2 is Y + 1, cell(X2,Y2).

% Count the number of living neighbours for every cell, at every T
nbNeigh(X,Y,T,N) :- findall((X2,Y2), (neighbour(X,Y,X2,Y2), alive(X2,Y2,T)), List), length(List,N).

% ProbLife rules
alive(X,Y,0) :- initAlive(X,Y).
0.9::alive(X,Y,T) :- T > 0, Tp is T - 1, alive(X,Y,Tp), nbNeigh(X,Y,Tp,3).
0.9::alive(X,Y,T) :- T > 0, Tp is T - 1, alive(X,Y,Tp), nbNeigh(X,Y,Tp,2).
0.8::alive(X,Y,T) :- T > 0, Tp is T - 1, \+alive(X,Y,Tp), nbNeigh(X,Y,Tp,3).


% Query the state at T=1 and T=2.
query(alive(X,Y,1)) :- cell(X, Y).
query(alive(X,Y,2)) :- cell(X,Y).


