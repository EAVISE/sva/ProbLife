# ProbLife

## What is ProbLife?

ProbLife is a probabilistic extension of the well-known [Game of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life).
The original GoL contains rules such as "If a cell has exactly 3 neighbours, it can survive".
ProbLife extends these rules with probabilities: e.g., "If a cell has exactly 3 neighbours, it has a 90% chance to survive".
More information on ProbLife can be found in our publication, once it's published.

## How can I run ProbLife?

This repo contains an example implementation of a ProbLife ruleset in the [ProbLog System](https://dtai.cs.kuleuven.be/problog/index.html), a probabilistic reasoning engine.
For more information, see `example_problife.pl`.
In order to run it, install ProbLog via pip and simply run it.

```
$ pip3 install problog
$ problog example_problife.pl
```

Besides the ProbLog implementation, this repo also contains a basic web-based interface for toying around with ProbLife.
In it, you can:

* modify the grid size;
* quickly set an initial state;
* have ProbLog generate `x` states for you, to watch the initial state evolve;
* view a cell's probability to live, by clicking on it;
* play around with various ProbLife rulesets.

The default ruleset used is the same as standard GoL, but with a 90% survival chance and a 80% birth chance.
To run the server, simply execute the following command and open your browser at `localhost:8000`. The rest should be self-explanatory.

```
$ python3 server.py
```


## Do you have any example images?

Well, yes!
ProbLife can be seen as a form of rule-based generative art.
For example, the following images have been generated using ProbLife (initial state on the left, and time increasing towards the right):

![unamused_tree.png](./Img/unamused_tree.png)

![cold_water.png](./Img/cold_water.png)

![fata_morgana.png](./Img/fata_morgana.png)
