class Rule {
  constructor(previousAlive, nbNeighbours, probability) {
    this.previousAlive = previousAlive;
    this.nbNeighbours = nbNeighbours;
    this.probability = probability;
  }

  toSentence() {
    let str = "Cell lives if it was "
    if (this.previousAlive) {
      str += "alive"
    } else {
      str += "dead"
    }
    str += " in the previous generation and had " + String(this.nbNeighbours) + " neighbours:"
    return str
  }

  toDict() {
    return {
            'previousAlive': this.previousAlive,
            'nbNeighbours': this.nbNeighbours,
            'probability': this.probability
           }
  }

}


class Grid {
  constructor() {
    // Keep an array of states.
    this.states = [
                    [0, 1, 0, 0, 1, 0, 0, 1, 0]
                  ];

    // Keep an array of rules.
    this.rules = [new Rule(false, 3, 0.8), new Rule(true, 2, 0.9), new Rule(true, 3, 0.9)];

    // Grid information.
    this.cols = 3;
    this.rows = 3;
    this.duration = 2;
    this.currentState = 0;
    this.loop = false;
    this.showProbs = [];

    // HTML elements.
    this.rulesDiv = document.getElementById("rulesDiv");
    this.generationSlider = document.getElementById("generationSlider");
    this.generationNb = document.getElementById("generationNb");
    this.minGenerationNb = document.getElementById("minGenerationNb");
    this.maxGenerationNb = document.getElementById("maxGenerationNb");
    this.autoRunCheckbox = document.getElementById("autoRunCheckbox");
    this.generationNb.innerHTML = 0;
    this.minGenerationNb.innerHTML = 0;
    this.maxGenerationNb.innerHTML = 0;
    this.autoRunCheckbox.checked = false;

    this.updateSliders();
  }

  updateSliders() {
    let html = ""
    for (var i = 0; i < this.rules.length; i++) {
      const rule = this.rules[i].toSentence();
      const prob = this.rules[i].probability;
      const idx = String(i);

      //html += '<input id="probSlider' + String(i) + '" type="range" min="0" max="1" value="' + String(prob) + '" step="0.01" oninput="probSlider(' + String(i) + ');"/>';
      //html += '<button type="button" id="ruleButton' + String(i) + '" onclick="removeRule(' + String(i) + ');">remove</button></br>';
      html += "Cell lives if it was " + '<select name="ruleAlive" id="ruleAlive' + idx +'" onchange="aliveSelect('+idx+');"><option value="alive">alive</option><option value="dead" onchange="aliveSelect('+ idx + ');">dead</option></select>'
      html += ' in the previous generation and had ' + '<select name="ruleNb" id="ruleNb' + idx + '" onchange="nbSelect(' + idx + ');"><option value="0">0</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option></select>'
      html += ' neighbours: '
      html += '<input type=text" id="probInput' + String(i) + '"size="3" value="' + String(prob) + '" oninput="probInput(' + String(i) + ');"/>';
      html += '<input id="probSlider' + String(i) + '" type="range" min="0" max="1" value="' + String(prob) + '" step="0.01" oninput="probSlider(' + String(i) + ');"/><br/>';
    }


    this.rulesDiv.innerHTML = html;

    // Now set them to the correct values.
    for (var i = 0; i < this.rules.length; i++) {
      const idx = String(i);
      if (this.rules[i]['previousAlive']) {
        document.getElementById('ruleAlive' + idx).value = 'alive';
      } else {
        document.getElementById('ruleAlive' + idx).value = 'dead';
      }
      document.getElementById('ruleNb'+idx).value = String(this.rules[i]['nbNeighbours']);
    }
  }

  updateCurrentState(nb) {
    this.currentState = nb;
    this.generationNb.innerHTML = nb;
    this.generationSlider.value = nb;
  }



  /*
   * Function called by the canvas whenever a mouseDown event is registered.
   */
  checkMouseDown(mouseX, mouseY) {
    const cellSize = 81.25;
    const leftMargin = (650 - cellSize * grid.cols)/2;
    const topMargin = 0;
    const rightMargin = 650 - leftMargin;
    const botMargin = cellSize * grid.rows;
    if (leftMargin < mouseX && mouseX < rightMargin && topMargin < mouseY && mouseY < botMargin) {
    // If someone clicks in the grid, find out which cell is clicked and toggle it.
      const xLoc = Math.floor((mouseX - leftMargin) / cellSize);
      const yLoc = Math.floor((mouseY - topMargin) / cellSize);
      const idx = yLoc * grid.cols + xLoc;
      if (grid.currentState === 0 && grid.states[grid.currentState][idx]) {
        grid.states[grid.currentState][idx] = 0;
      } else if (grid.currentState === 0) {
        grid.states[grid.currentState][idx] = 1;
      } else {
        if (grid.showProbs.includes(idx)) {
          grid.showProbs.pop(idx);
        } else {
          grid.showProbs.push(idx);
        }
      }
    }

  }
  /*
   * Function called by the canvas whenever a mouseUp event is registered.
   */
  checkMouseUp(mouseX, mouseY) {
  }

  /*
   * Function called by canvas whenever a mouseMove event is registered.
   */
  checkMouseMove(mouseX, mouseY) {

  }

  updateWithJson(json) {
    this.states = json['states']
    this.loop = true;
    //document.getElementById("generationSlider").max = this.states.length;
    this.generationSlider.max = this.states.length - 1;
    this.minGenerationNb.innerHTML = 0
    this.maxGenerationNb.innerHTML = this.states.length - 1;
    this.autoRunCheckbox.checked = false;
  }

  reset() {
    this.loop = false;
    this.states = [new Array(this.cols * this.rows).fill(0)];
    this.updateCurrentState(0);
    this.generationSlider.max = 0;
    this.minGenerationNb.innerHTML = 0;
    this.maxGenerationNb.innerHTML = 0;
    this.autoRunCheckbox.checked = false;
  }
}

function changeWidth() {
  grid.cols = parseInt(document.getElementById("width").value);
  grid.reset();
}

function changeHeight() {
  grid.rows = parseInt(document.getElementById("height").value);
  grid.reset();
}

function changeDuration() {
  grid.duration = parseInt(document.getElementById("duration").value);
}

function prevState() {
  if (grid.currentState > 0) {
    grid.updateCurrentState(grid.currentState - 1);
  }
}

function nextState() {
  if (grid.states.length-1 > grid.currentState) {
    grid.updateCurrentState(grid.currentState + 1);
  }
}

function cycle(start=true) {
  if (document.getElementById("autoRunCheckbox").checked) {
    grid.loop = true;
    runCycle();
  } else {
    grid.loop = false;
  }
}

function runCycle() {
    if (grid.loop) {
      if (grid.currentState < grid.states.length - 1) {
        grid.updateCurrentState(grid.currentState + 1);
      } else {
        grid.updateCurrentState(0);
      }
      setTimeout(runCycle, 1500);
    }
  }

function resetState() {
  grid.reset();
}

function sliderUpdated() {
  grid.updateCurrentState(parseInt(grid.generationSlider.value));
}

function probSlider(idx) {
  prob = document.getElementById("probSlider"+idx).value;
  grid.rules[idx].probability = prob;
  document.getElementById("probInput"+idx).value = prob;
}

function probInput(idx) {
  prob = document.getElementById("probInput"+idx).value;
  grid.rules[idx].probability = prob;
  document.getElementById('probSlider'+idx).value = prob;
}

function nbSelect(idx) {
  grid.rules[idx]['nbNeighbours'] = parseInt(document.getElementById('ruleNb'+String(idx)).value);

}

function aliveSelect(idx) {
  if (document.getElementById('ruleAlive'+String(idx)).value === 'alive') {
    grid.rules[idx]['previousAlive'] = true;
  } else {
    grid.rules[idx]['previousAlive'] = false;
  }
}

function delRule(idx) {
    grid.rules.pop();
    grid.updateSliders();
}

function addRule(idx) {
  const newRule = new Rule(false, 0, 0);
  grid.rules.push(newRule);
  grid.updateSliders();
}

function generateJson() {
  const json = {
    'states': grid.states,
    'cols': grid.cols,
    'rows': grid.rows,
    'rules': grid.rules,
    'time': grid.duration + 1
  }
  return json;
}


/*
 * Send post request to server.
 */
function doPost(json) {
  // Make the loading bar appear.
  document.getElementById('loader').style.visibility = 'visible';

  const url = "localhost";
  //var params = "lorem=ipsum&name=alpha";
  var xhr = new XMLHttpRequest();
  xhr.open("POST", '', true);
  xhr.setRequestHeader('Content-type', 'application/json; charset=UTF-8');

  xhr.send(JSON.stringify(json));
  console.log(json);
  xhr.onloadend = function () {
    if (xhr.readyState === 4 && xhr.status === 200) {
      console.log(xhr.responseText);
      const json = JSON.parse(xhr.responseText);
      grid.updateWithJson(json);
      console.log(json);
    }
    console.log('Done!');
    document.getElementById('loader').style.visibility = 'hidden';
  };
  console.log('Sent POST');

  //Send the proper header information along with the request
  //xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  //
  //xhr.send(params);
  //
}

function runProblog() {
  grid.currentState = 0;
  const json = generateJson();
  doPost(json);
}

// https://stackoverflow.com/questions/17242144/javascript-convert-hsb-hsv-color-to-rgb-accurately
// input: h in [0,360] and s,v in [0,1] - output: r,g,b in [0,1]
function hsv2rgb(h,s,v) {
  let f = (n,k=(n+h/60)%6) => v - v*s*Math.max( Math.min(k,4-k,1), 0);
  return [f(5)*255,f(3)*255,f(1)*255];
}

function drawGrid() {

  const cellSize = 81.25;
  ctx.beginPath();
  ctx.lineWidth = '2';
  ctx.strokeStyle = 'black';

  const leftMargin = (650 - cellSize * grid.cols)/2;
  const topMargin = 0;
  const rightMargin = 650 - leftMargin;
  const botMargin = (cellSize * grid.rows);

  // Draw horizontal gridlines.
  for (var i = 0; i < grid.rows + 1; i++) {
    const y = topMargin + i * cellSize;
    ctx.moveTo(leftMargin, y);
    ctx.lineTo(rightMargin, y);
    ctx.stroke();
  }
  // Draw vertical gridlines.
  for (var i = 0; i < grid.cols + 1; i++) {
    const x = leftMargin + i * cellSize;
    ctx.moveTo(x, topMargin);
    ctx.lineTo(x, botMargin);
    ctx.stroke();
  }

  // Draw the cells.
  for (var i = 0; i < grid.states[grid.currentState].length; i++) {
    const cell = grid.states[grid.currentState][i];
    if (cell > 0.03) {
      const x = (i % grid.cols) * cellSize + leftMargin;
      const y = Math.floor(i / grid.cols) * cellSize + topMargin;

      const color = hsv2rgb((1-cell)*180, 1, 1);
      ctx.fillStyle = "rgb("+color[0] + "," + color[1] + ","+ color[2] + ")";
      //console.log("rgb("+color[0] + "," + color[1] + ",", color[2] + ")");
      //ctx.fillStyle = "rgb(255,0,0)"
      ctx.fillRect(x, y, cellSize, cellSize);

      if (grid.showProbs.includes(i)) {
        ctx.font = "20px Arial";
        ctx.fillStyle = 'white';
        ctx.fillText(cell, x+cellSize/3, y+cellSize/2);
      }
    }
  }

  //// Draw "board" and "player" text labels.
  //ctx.fillText('Player', 10, 520);
}


function onMouseDown(event) {
  const rect = canvas.getBoundingClientRect();
  const mouseX = event.clientX - rect.left;
  const mouseY = event.clientY - rect.top;
  grid.checkMouseDown(mouseX, mouseY);
}

function onMouseUp(event) {
  const rect = canvas.getBoundingClientRect();
  const mouseX = event.clientX - rect.left;
  const mouseY = event.clientY - rect.top;
  grid.checkMouseUp(mouseX, mouseY);
}

function onMouseMove(event) {
  const rect = canvas.getBoundingClientRect();
  const mouseX = event.clientX - rect.left;
  const mouseY = event.clientY - rect.top;
  grid.checkMouseMove(mouseX, mouseY);
}

/*
 * Functions to handle the touch events.
 * We simply convert them to an equivalent mouse event, and let the event dispatcher handle the rest.
 *
 */
function onTouchStart(event) {
  const mouseEvent = new MouseEvent("mousedown", {
    clientX: event.touches[0].clientX,
    clientY: event.touches[0].clientY
  });
  canvas.dispatchEvent(mouseEvent);
}

function onTouchEnd(event) {
  const mouseEvent = new MouseEvent("mouseup", {
    clientX: event.touches[0].clientX,
    clientY: event.touches[0].clientY
  });
  canvas.dispatchEvent(mouseEvent);
}

function onTouchMove(event) {
  const mouseEvent = new MouseEvent("mousemove", {
    clientX: event.touches[0].clientX,
    clientY: event.touches[0].clientY
  });
  canvas.dispatchEvent(mouseEvent);
}

/*
 * Function to prevent the webpage from scrolling, to be used when touching the canvas on mobile.
 */
function preventScrolling(event) {
  alert('a');
  if (event.target == canvas) {
    alert('b');
    e.preventDefault();
  }
}

function draw() {
  // Clear all (TODO: optimize this!)
  ctx.clearRect(0, 0, 650, 650);

  // Draw the field lines.
  drawGrid();

  // Keep drawing.
  window.requestAnimationFrame(draw);
}

function main(tframe) {
  // Fetch canvas information.
  canvas = document.getElementById('canvas');
  ctx = canvas.getContext('2d');

  // Set the event listeners for interacting with the canvas.
  canvas.onmousedown = onMouseDown;
  canvas.onmouseup = onMouseUp;
  canvas.onmousemove = onMouseMove;

  canvas.ontouchstart = onTouchStart;
  canvas.ontouchend = onTouchEnd;
  canvas.ontouchmove = onTouchMove;

  // We also want to prevent scrolling when touching the canvas on mobile.
  // Thanks to: http://bencentra.com/code/2014/12/05/html5-canvas-touch-events.html
  document.body.addEventListener("touchstart", function (e) {
    if (e.target == canvas) {
      e.preventDefault();
    }
  }, false);
  document.body.addEventListener("touchend", function (e) {
    if (e.target == canvas) {
      e.preventDefault();
    }
  }, false);
  document.body.addEventListener("touchmove", function (e) {
    if (e.target == canvas) {
      e.preventDefault();
    }
  }, false);

  // Create a new game.
  grid = new Grid();

  // Start drawing.
  window.requestAnimationFrame(draw);
}


main();
