from http.server import HTTPServer, BaseHTTPRequestHandler

from io import BytesIO
import json
import os
from problog.program import PrologString
from problog.core import ProbLog
from problog import get_evaluatable
from problog.util import start_timer, stop_timer


def generate_pl(width, height, time, rules, alive=[], random_start=0):
    """
    Generate a ProbLog specification for ProbLife.
    Either supply the starting state *alive*, or the value for random start.

    :arg width: width of the grid
    :type width: int
    :arg height: height of the grid
    :type height: int
    :arg time: the nb of generations (including starting gen.)
    :type time: int
    :arg alive: List representing a starting state.
    :type alive: List[float]
    :arg random_start: Number to denote random life chance of the first state
    :type random_start: float
    """
    # Generate the width and height facts.
    spec = ""
    for i in range(width):
        spec += f"row({i}). "
    spec += "\n"

    for i in range(height):
        spec += f"col({i}). "
    spec += "\n"
    spec += "cell(X,Y) :- row(X), col(Y).\n"

    # Generate the start state.
    if alive:
        for i, cell in enumerate(alive):
            x = i % width
            y = i // width
            round_cell = round(cell, 4)
            if round_cell > 0.05:
                spec += f"{round_cell}::initAlive({x},{y}).\n"

    spec += """
        neighbour(X,Y,X2,Y2) :- X2 is X - 1, Y2 is Y - 1, cell(X2,Y2).
        neighbour(X,Y,X2,Y) :- X2 is X - 1, cell(X2,Y).
        neighbour(X,Y,X2,Y2) :- X2 is X - 1, Y2 is Y + 1, cell(X2,Y2).
        neighbour(X,Y,X,Y2) :- Y2 is Y - 1, cell(X,Y2).
        neighbour(X,Y,X,Y2) :- Y2 is Y + 1, cell(X,Y2).
        neighbour(X,Y,X2,Y2) :- X2 is X + 1, Y2 is Y - 1, cell(X2,Y2).
        neighbour(X,Y,X2,Y) :- X2 is X + 1, cell(X2,Y).
        neighbour(X,Y,X2,Y2) :- X2 is X + 1, Y2 is Y + 1, cell(X2,Y2).
        nbNeigh(X,Y,T,N) :- findall((X2,Y2), (neighbour(X,Y,X2,Y2), alive(X2,Y2,T)), List), length(List,N).
        alive(X,Y,0) :- initAlive(X,Y).\n
        """

    # Add the custom rules, based on prob, nb of neighbours and if cell was
    # alive previously.
    for rule in rules:
        if rule['previousAlive']:
            alive = "alive(X, Y, Tp)"
        else:
            alive = "\+alive(X, Y, Tp)"
        spec += (f"{rule['probability']}::alive(X,Y,T) :- T > 0, Tp is T - 1,"
                 f"{alive}, nbNeigh(X,Y,Tp,{rule['nbNeighbours']}).\n")


    # Query the result!
    spec += "query(alive(X,Y,1)) :- row(X), col(Y).\n"

    return spec

def run_problog(cols, rows, alive, rules):
    states = [alive]
    # Keep creating generations until they are extinct.
    # This threshold could be changed, but 0.05 seems low enough.
    start_timer(timeout=50)
    balance = 5
    try:
        while max(alive) > 0.05:
            # Generate and run a ProbLog spec.
            spec = generate_pl(cols, rows, 0, rules, alive=alive)
            p = PrologString(spec)
            table = get_evaluatable().create_from(p).evaluate()

            # Convert keys to strings instead of ProbLog `Terms`.
            table = {str(x): y for x, y in table.items()}
            end_alive = []
            # Read out unordered dict in correct order.
            # (We should probably just sort it beforehand.)
            print(table)
            for i in range(rows*cols):
                x = i % cols
                y = i // cols
                key = f"alive({x},{y},1)"
                end_alive.append(table[key])
            states.append(end_alive)
            alive = end_alive

            # Normalize to something
            # total = sum(end_alive)
            # factor = (balance - total)/len([x for x in alive if x > 0.05])
            # print(factor)
            # alive = [max(min(1, x+factor), 0) for x in end_alive]

            # Normalize to 1
            # total = sum(end_alive)
            # factor = balance/total
            # alive = [min(1, x*factor) for x in end_alive]
            print(alive)
    except KeyboardInterrupt:
        pass
    stop_timer()

    return states


class SimpleHTTPRequestHandler(BaseHTTPRequestHandler):

    def do_GET(self):
        if self.path == '/':
            path = 'problife.html'
            mime = 'text/html'
        elif self.path == '/problife.css':
            path = './problife.css'
            mime = 'text/css'
        else:
            # Strip the '/' in the front.
            path = self.path[1:]
            mime = 'text/css'
        self.send_response(200)
        self.send_header('Content-type', mime)
        self.end_headers()
        with open(path, 'r') as fp:
            html = fp.read()
        # self.wfile.write(b'Hello, world!')
        self.wfile.write(bytes(html, 'utf-8'))

    def do_POST(self):
        # Receive POST request.
        content_length = int(self.headers['Content-Length'])
        body = self.rfile.read(content_length)

        # Decode the json into a dict
        data = json.loads(body)
        print(data)

        states = run_problog(data['cols'], data['rows'], data['states'][0], data['rules'])
        response_json = {'states': states, 'cols': data['cols'], 'rows': data['rows']}

        # Send response.
        self.send_response(200)
        self.end_headers()
        response = BytesIO()
        response.write(bytes(json.dumps(response_json), 'utf-8'))
        self.wfile.write(response.getvalue())


if __name__ == "__main__":
    if 'PORT' in os.environ:
        port = int(os.environ['PORT'])
    else:
        port = 8000
    httpd = HTTPServer(('0.0.0.0', port), SimpleHTTPRequestHandler)
    print('Started http server at http://localhost:8000')
    httpd.serve_forever()
